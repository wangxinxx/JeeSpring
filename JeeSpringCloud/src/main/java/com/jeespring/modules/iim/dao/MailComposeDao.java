/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeespring.modules.iim.dao;

import com.jeespring.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.iim.entity.MailCompose;

/**
 * 发件箱DAO接口
 * @author jeertd
 * @version 2015-11-15
 */
@Mapper
public interface MailComposeDao extends CrudDao<MailCompose> {
	public int getCount(MailCompose entity);
}