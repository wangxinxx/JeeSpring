/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeespring.modules.test.dao.tree;

import com.jeespring.common.persistence.TreeDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.test.entity.tree.TestTree;

/**
 * 组织机构DAO接口
 * @author liugf
 * @version 2016-01-15
 */
@Mapper
public interface TestTreeDao extends TreeDao<TestTree> {
	
}