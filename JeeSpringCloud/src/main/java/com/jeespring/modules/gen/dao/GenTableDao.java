package com.jeespring.modules.gen.dao;

import com.jeespring.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.gen.entity.GenTable;

import org.apache.ibatis.annotations.Param;

@Mapper
public abstract interface GenTableDao
  extends CrudDao<GenTable>
{
  public abstract int buildTable(@Param("sql") String paramString);
}
