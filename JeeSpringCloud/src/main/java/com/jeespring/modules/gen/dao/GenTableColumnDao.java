package com.jeespring.modules.gen.dao;

import com.jeespring.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.gen.entity.GenTable;
import com.jeespring.modules.gen.entity.GenTableColumn;

@Mapper
public abstract interface GenTableColumnDao
  extends CrudDao<GenTableColumn>
{
  public abstract void deleteByGenTable(GenTable paramGenTable);
}
