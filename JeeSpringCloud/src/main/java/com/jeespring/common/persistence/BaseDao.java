/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeespring">jeespring</a> All rights reserved.
 */
package com.jeespring.common.persistence;

/**
 * DAO支持类实现
 * @author ThinkGem
 * @version 2014-05-16
 */
public interface BaseDao {

}