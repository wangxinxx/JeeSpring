package com.jeespring;

import com.jeespring.common.websocket.WebSockertFilter;
import com.jeespring.modules.sys.service.SystemService;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * wolfking-jeespring
 * springboot的启动类
 * Created by wolfking(赵伟伟)
 * Created on 2017/1/8 16:20
 * Mail zww199009@163.com
 */
@EnableCaching
@SpringBootApplication
@ServletComponentScan("com.jeespring")
@ComponentScan(value = "com.jeespring",lazyInit = true)
public class JeeSpringDriver {
    public static void main(String[] args) {
    	
    	WebSockertFilter w=new WebSockertFilter();
    	w.startWebsocketChatServer();
        new SpringApplicationBuilder(JeeSpringDriver.class).web(true).run(args);
        SystemService.printKeyLoadMessage();
    }
}
