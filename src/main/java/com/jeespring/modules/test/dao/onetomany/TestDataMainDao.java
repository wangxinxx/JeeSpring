/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeespring.modules.test.dao.onetomany;

import com.jeespring.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.test.entity.onetomany.TestDataMain;

/**
 * 票务代理DAO接口
 * @author liugf
 * @version 2016-01-15
 */
@Mapper
public interface TestDataMainDao extends CrudDao<TestDataMain> {
	
}