/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeespring.modules.sys.dao;

import org.apache.ibatis.annotations.Mapper;

import com.jeespring.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.sys.entity.Log;

/**
 * 日志DAO接口
 * @author jeertd
 * @version 2014-05-16
 */
@Mapper
public interface LogDao extends CrudDao<Log> {

	public void empty();
}
