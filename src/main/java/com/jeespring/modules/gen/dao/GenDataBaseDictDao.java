package com.jeespring.modules.gen.dao;

import com.jeespring.common.persistence.CrudDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.gen.entity.GenTable;
import com.jeespring.modules.gen.entity.GenTableColumn;

import java.util.List;

@Mapper
public abstract interface GenDataBaseDictDao
  extends CrudDao<GenTableColumn>
{
  public abstract List<GenTable> findTableList(GenTable paramGenTable);
  
  public abstract List<GenTableColumn> findTableColumnList(GenTable paramGenTable);
  
  public abstract List<String> findTablePK(GenTable paramGenTable);
}