package com.jeertd.platform.gen.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.gen.entity.GenTable;
import com.jeertd.platform.gen.entity.GenTableColumn;

import java.util.List;

@RtdMyBatisDao
public abstract interface GenDataBaseDictDao
  extends RtdRepositoryDao<GenTableColumn>
{
  public abstract List<GenTable> findTableList(GenTable paramGenTable);
  
  public abstract List<GenTableColumn> findTableColumnList(GenTable paramGenTable);
  
  public abstract List<String> findTablePK(GenTable paramGenTable);
}