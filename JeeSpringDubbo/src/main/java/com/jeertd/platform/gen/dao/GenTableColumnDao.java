package com.jeertd.platform.gen.dao;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.gen.entity.GenTable;
import com.jeertd.platform.gen.entity.GenTableColumn;

@RtdMyBatisDao
public abstract interface GenTableColumnDao
  extends RtdRepositoryDao<GenTableColumn>
{
  public abstract void deleteByGenTable(GenTable paramGenTable);
}
