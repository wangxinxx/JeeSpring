/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.iim.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeertd.core.orm.RtdPage;
import com.jeertd.core.service.RtdRepositoryService;
import com.jeertd.platform.iim.dao.MailBoxDao;
import com.jeertd.platform.iim.entity.MailBox;
import com.jeertd.platform.iim.entity.MailPage;

/**
 * 收件箱Service
 * @author jeertd
 * @version 2015-11-13
 */
@Service
@Transactional(readOnly = true)
public class MailBoxService extends RtdRepositoryService<MailBoxDao, MailBox> {

	@Autowired
	private MailBoxDao mailBoxDao;
	public MailBox get(String id) {
		return super.get(id);
	}
	
	public List<MailBox> findList(MailBox mailBox) {
		return super.findList(mailBox);
	}
	
	public RtdPage<MailBox> findPage(MailPage<MailBox> page, MailBox mailBox) {
		return super.findPage(page, mailBox);
	}
	
	@Transactional(readOnly = false)
	public void save(MailBox mailBox) {
		super.save(mailBox);
	}
	
	@Transactional(readOnly = false)
	public void delete(MailBox mailBox) {
		super.delete(mailBox);
	}
	
	public int getCount(MailBox mailBox) {
		return mailBoxDao.getCount(mailBox);
	}
	
}