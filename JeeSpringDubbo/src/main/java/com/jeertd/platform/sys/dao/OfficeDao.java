/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.sys.dao;

import com.jeertd.core.orm.RtdTreeDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.sys.entity.Office;

/**
 * 机构DAO接口
 * @author jeertd
 * @version 2014-05-16
 */
@RtdMyBatisDao
public interface OfficeDao extends RtdTreeDao<Office> {
	
	public Office getByCode(String code);
}
