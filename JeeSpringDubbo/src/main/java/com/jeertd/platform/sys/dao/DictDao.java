/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.sys.dao;

import java.util.List;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.sys.entity.Dict;

/**
 * 字典DAO接口
 * @author jeertd
 * @version 2014-05-16
 */
@RtdMyBatisDao
public interface DictDao extends RtdRepositoryDao<Dict> {

	public List<String> findTypeList(Dict dict);
	
}
