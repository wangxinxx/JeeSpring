/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeertd.core.common.utils.DateUtils;
import com.jeertd.core.orm.RtdPage;
import com.jeertd.core.service.RtdRepositoryService;
import com.jeertd.platform.sys.dao.LogDao;
import com.jeertd.platform.sys.entity.Log;

/**
 * 日志Service
 * @author jeertd
 * @version 2014-05-16
 */
@Service
@Transactional(readOnly = true)
public class LogService extends RtdRepositoryService<LogDao, Log> {

	@Autowired
	private LogDao logDao;
	
	public RtdPage<Log> findPage(RtdPage<Log> page, Log log) {
		
		// 设置默认时间范围，默认当前月
		if (log.getBeginDate() == null){
			log.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
		}
		if (log.getEndDate() == null){
			log.setEndDate(DateUtils.addMonths(log.getBeginDate(), 1));
		}
		
		return super.findPage(page, log);
		
	}
	
	/**
	 * 删除全部数据
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void empty(){
		
		logDao.empty();
	}
	
}
