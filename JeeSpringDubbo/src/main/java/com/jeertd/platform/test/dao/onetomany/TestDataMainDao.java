/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.platform.test.dao.onetomany;

import com.jeertd.core.orm.RtdRepositoryDao;
import com.jeertd.core.orm.annotation.RtdMyBatisDao;
import com.jeertd.platform.test.entity.onetomany.TestDataMain;

/**
 * 票务代理DAO接口
 * @author liugf
 * @version 2016-01-15
 */
@RtdMyBatisDao
public interface TestDataMainDao extends RtdRepositoryDao<TestDataMain> {
	
}