/**
 * Copyright &copy; 2012-2016 <a href="https://git.oschina.net/guanshijiehnan/JeeRTD">JeeSite</a> All rights reserved.
 */
package com.jeertd.rtd.api.redis;

import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;

/**
 * 单表生成Servicez
 * @author ThinkGem
 * @version 2017-05-04
 */
public interface RedisDubbo {
	public String get(String keyGet,String keyPost);
	public String set(String keyGet,String keyPost, String valueGet,@FormParam("value") String valuePost,int cacheSecondsGet, int cacheSecondsPost);
	public long delete(String keyGet,String keyPost);
	
}