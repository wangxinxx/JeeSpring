/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.core.service;

/**
 * Service层公用的Exception, 从由Spring管理事务的函数中抛出时会触发事务回滚.
 * @author jeertd
 */
public class RtdServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RtdServiceException() {
		super();
	}

	public RtdServiceException(String message) {
		super(message);
	}

	public RtdServiceException(Throwable cause) {
		super(cause);
	}

	public RtdServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
