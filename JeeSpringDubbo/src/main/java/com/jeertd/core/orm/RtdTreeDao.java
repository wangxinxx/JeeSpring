/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.core.orm;

import java.util.List;

/**
 * DAO支持类实现
 * @author jeertd
 * @version 2014-05-16
 * @param <T>
 */
public interface RtdTreeDao<T extends RtdTreeEntity<T>> extends RtdRepositoryDao<T> {

	/**
	 * 找到所有子节点
	 * @param entity
	 * @return
	 */
	public List<T> findByParentIdsLike(T entity);

	/**
	 * 更新所有父节点字段
	 * @param entity
	 * @return
	 */
	public int updateParentIds(T entity);
	
}