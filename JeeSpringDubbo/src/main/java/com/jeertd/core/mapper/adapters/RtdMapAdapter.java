/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeertd.org/">jeertd</a> All rights reserved.
 */
package com.jeertd.core.mapper.adapters;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class RtdMapAdapter extends XmlAdapter<RtdMapConvertor, Map<String, Object>> {  
	  
    @Override  
    public RtdMapConvertor marshal(Map<String, Object> map) throws Exception {  
        RtdMapConvertor convertor = new RtdMapConvertor();  
        for (Map.Entry<String, Object> entry : map.entrySet()) {  
            RtdMapConvertor.MapEntry e = new RtdMapConvertor.MapEntry(entry);  
            convertor.addEntry(e);  
        }  
        return convertor;  
    }  
  
    @Override  
    public Map<String, Object> unmarshal(RtdMapConvertor map) throws Exception {  
        Map<String, Object> result = new HashMap<String, Object>();  
        for (RtdMapConvertor.MapEntry e : map.getEntries()) {  
            result.put(e.getKey(), e.getValue());  
        }  
        return result;  
    }
    
}  

